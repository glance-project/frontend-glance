import { createRouter, createWebHistory } from 'vue-router';
import { RouterName } from './constants';

import CatalogPage from '@/pages/users/Catalog/PageCatalog.vue';
import CardProduct from '@/pages/users/CardProduct.vue';
import BasketPage from '@/pages/users/BasketPage.vue';
import ProfilePage from '@/pages/users/ProfilePage.vue';
import Phones from '@/pages/users/Phones/PagePhones.vue';
import CardPhone from '@/pages/users/CardPhone/PagePhone.vue';
import Laptops from '@/pages/users/Laptops/PageLaptops.vue';
import Computers from '@/pages/users/Computers/PageComputers.vue';
import Tv from '@/pages/users/Tv/PageTv.vue';
import Tablets from '@/pages/users/Tablets/PageTablets.vue';
import Loudspeakers from '@/pages/users/Loudspeakers/PageLoudspeakers.vue';
import PageReview from '@/pages/users/PageReview/PageReview.vue';
import PageNotFound from '@/pages/users/PageNotFound.vue';

const routes = [
  {
    path: '/',
    name: RouterName.Catalog,
    component: CatalogPage
  },
  {
    path: '/basket',
    name: RouterName.Basket,
    component: BasketPage
  },
  {
    path: '/product',
    name: RouterName.Product,
    component: CardProduct
  },
  {
    path: '/profile',
    name: RouterName.Profile,
    component: ProfilePage
  },
  {
    path: '/phones',
    name: RouterName.Phones,
    component: Phones,
    // children: [
    //   {
    //     path: ':phone',
    //     name: RouterName.CardPhone,
    //     component: CardPhone
    //   }
    // ]
  },
  {
    path: '/phones/:phone',
    name: RouterName.CardPhone,
    component: CardPhone,
  },
  {
    path: '/phones/:phone/:id',
    name: RouterName.PageReview,
    component: PageReview,
  },
  {
    path: '/laptops',
    name: RouterName.Laptops,
    component: Laptops
  },
  {
    path: '/computers',
    name: RouterName.Computers,
    component: Computers
  },
  {
    path: '/tv',
    name: RouterName.Tv,
    component: Tv
  },
  {
    path: '/tablets',
    name: RouterName.Tablets,
    component: Tablets
  },
  {
    path: '/loudspeakers',
    name: RouterName.Loudspeakers,
    component: Loudspeakers
  },
  {
    path: '/404',
    name: RouterName.PageNotFound,
    component: PageNotFound
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router;
