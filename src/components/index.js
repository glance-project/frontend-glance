import UiAvatar from '@/components/UiAvatar.vue';
import UiButton from '@/components/UiButton.vue';
import UiCheckbox from '@/components/UiCheckbox.vue';
import UiCounter from '@/components/UiCounter.vue';
import UiHeart from '@/components/UiHeart.vue';
import UiIcon from '@/components/UiIcon.vue';
import UiInput from '@/components/UiInput.vue';
import UiMessage from '@/components/UiMessage.vue';
import UiModal from '@/components/UiModal.vue';
import UiSpinner from '@/components/UiSpinner.vue';
import UiSquare from '@/components/UiSquare.vue';
import UiStars from '@/components/UiStars.vue';

export default [
  UiAvatar,
  UiButton,
  UiCheckbox,
  UiCounter,
  UiHeart,
  UiIcon,
  UiInput,
  UiMessage,
  UiModal,
  UiSpinner,
  UiSquare,
  UiStars,
];
