import { defineStore } from 'pinia';
import { ref, computed } from 'vue';

export const useAuthStore = defineStore('auth', () =>  {
    const myTestAlert = ref('test text');
    const doubleCount = computed(() => myTestAlert.value + 'success');
    return {
        myTestAlert,
        doubleCount
    }
})
