import { createApp } from 'vue';
import App from './App.vue';
import components from '@/components';
import axios from 'axios';
import VueAxios from 'vue-axios';
import store from '@/store';
import router from '@/router';

import './assets/icons/icomoon/style.css';
import './assets/styles/main.scss';

const app = createApp(App);

components.forEach((component: any) => {
    app.component(component.__name, component);
});

app
    .use(VueAxios, axios)
    .use(store)
    .use(router)
    .mount('#app');
